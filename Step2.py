import numpy as np
import  pandas as pd
import  matplotlib
import  matplotlib.pyplot as plt
import seaborn as sns
import sklearn
from sklearn.linear_model import  LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import  roc_auc_score, plot_roc_curve, confusion_matrix
from sklearn.model_selection import KFold

data_path ="./data/creditcard.csv"
df = pd.read_csv(data_path)

normal = df[df.Class == 0 ].sample(frac=0.5, random_state=2020).reset_index(drop=True)
anomaly = df[df.Class == 1]

print(f"Normal : {normal.shape}") ## (142158, 31)
print(f"anomaly : {anomaly.shape}") ## (492, 31)

# x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=2020) ## data split code

normal_train, normal_test = train_test_split(normal, test_size= 0.2, random_state= 2020) ## X
anomaly_train, anomaly_test =train_test_split(anomaly, test_size=0.2, random_state=2020) ## Y

normal_train, normal_validate = train_test_split(normal_train, test_size= 0.25, random_state= 2020) ## X_val
anomaly_train, anomaly_validate =train_test_split(anomaly_train, test_size=0.25, random_state=2020) ## Y_val

x_train = pd.concat((normal_train, anomaly_train))  ## 정상세트와 이상세트(학습, 테스트, 검증)으 조합
x_test = pd.concat((normal_test, anomaly_test))
x_validate = pd.concat((normal_validate, anomaly_validate))

y_train = np.array(x_train["Class"])   ## X 세트의 클래스에 있는 데이터
y_test = np.array(x_test["Class"])
y_validate = np.array(x_validate["Class"])

x_train = x_train.drop("Class", axis =1)  ## 레이블을 직접 제공하면 모델학습에 방해되기에 삭제
x_test = x_test.drop("Class", axis =1)
x_validate = x_validate.drop("Class", axis =1)

print("Traing sets:\nx_train : {} y_train: {}".format(x_train.shape, y_train.shape))
print("\nTesting sets:\nx_test : {} y_test: {}".format(x_test.shape, y_test.shape))
print("\nValidation sets:\nx_validate : {} y_validate: {}".format(x_validate.shape, y_validate.shape))

## 데이터 표준화 작업

scaler = StandardScaler()
scaler.fit(pd.concat((normal, anomaly)).drop("Class", axis=1))

x_train= scaler.transform(x_train)
x_test= scaler.transform(x_test)
x_validate= scaler.transform(x_validate)

sk_model = LogisticRegression(random_state=None, max_iter=400, solver='newton-cg').fit(x_train, y_train)

eval_acc = sk_model.score(x_test,y_test)

preds= sk_model.predict(x_test)
auc_score = roc_auc_score(y_test, preds)

print(f"Auc Score : {auc_score:.3%}")
print(f"Eval Accuracy : {eval_acc:.3%}")

roc_plot = plot_roc_curve(sk_model, x_test, y_test, name='Scikit-learn ROC Curve')
plt.show()
# plt.close()

conf_matrix = confusion_matrix(y_test, preds)
ax = sns.heatmap(conf_matrix, annot=True, fmt='g')
ax.invert_xaxis()
ax.invert_yaxis()
plt.ylabel('Actual')
plt.xlabel('Predicted')
plt.show()

## 모델 검증

anomaly_weights = [1, 5, 10, 15]

num_folds = 5
kfold = KFold(n_splits=num_folds, shuffle=True, random_state=2020)

logs =[]
for f in range(len(anomaly_weights)):
    fold = 1
    accuracies = []
    auc_scores = []
    for train, test in kfold.split(x_validate, y_validate):
        weight = anomaly_weights[f]
        class_weights= { 0:1, 1:weight}
        sk_model= LogisticRegression(random_state=None, max_iter=400, solver='newton-cg', class_weight=class_weights).fit(x_validate[train],y_validate[train
        ])

    for h in range(40):
        print('-', end="")
        print(f"\nfold {fold} \nAnomaly Weight: {weight}")
        eval_acc= sk_model.score(x_validate[test], y_validate[test])
        preds = sk_model.predict(x_validate[test])
        try:
            auc_score = roc_auc_score(y_validate[test], preds)
        except:
            auc_score=-1
        print(" AUC: {} \neval_acc: {}".format(auc_score, eval_acc))
        accuracies.append(eval_acc)
        auc_scores.append(auc_score)
        log = [sk_model, x_validate[test], y_validate[test], preds]
        logs.append(log)
        fold = fold + 1
    print("\nAverages: ")
    print("Acc : ", np.mean(accuracies))
    print("AUC : ", np.mean(auc_scores))
    print("Best :" )
    print("Acc : ", np.max(accuracies))
    print("AUC : ", np.max(auc_scores))

sk_model, x_val, y_val, preds = logs[11]
roc_plot = plot_roc_curve(sk_model, x_val, y_val, name='ScikitlearnROC Curve')
plt.show()