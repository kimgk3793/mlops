import  numpy as np
import  pandas as pd
import matplotlib.pyplot as plt
from pylab import rcParams

rcParams['figure.figsize'] = 14 , 8

data_path = "./data/creditcard.csv"
df = pd.read_csv(data_path)

data_head = df.head() ## 데이터 내용 확인
data_describe = df.describe() ## 데이터에서 각 칼럼에 있는 데이터 통계 요약
# print(df.shape) == (284807, 31)

## 데이터 분석을 하기전 데이터의 구성을 확인한다.

anomalies = df[df.Class == 1] # 부정 데이터
normal = df[df.Class == 0] # 정상 데이터

print(f"Anomalies : {anomalies.shape}") ## (492, 31)
print(f"Normal : {normal.shape}") ## (284315, 31)    --> 데이터가 지나치게 편향

### Plotting ####

def plot_histogram(df, bins, column, log_scale = False):
    bins = 100
    anomalies = df[df.Class == 1]
    normal = df[df.Class == 0]
    fig, (ax1, ax2) = plt.subplot(2, 1, sharex=True)
    fig.subtitle(f'Counts of {column} by Class')
    ax1.hist(anomalies[column], bins= bins, color="red")
    ax1.set_title("Anomaly")
    ax2.hist(normal[column], bins= bins, color="orange")
    ax2.set_title("Normal")
    plt.xlabel(f"{column}")
    plt.ylabel("Count")
    if log_scale:
        plt.yscale('log')
    plt.xlim((np.min(df[column]), np.max(df[column])))
    plt.show()

def plot_scatter(df, x_col, y_col, sharey= False):
    anomalies = df[df.Class == 1]
    normal = df[df.Class == 0]
    fig, (ax1, ax2) = plt.subplot(2, 1, sharex=sharey)
    fig.subtitle(f'{y_col} over {x_col} by Class')
    ax1.scatter(anomalies[x_col], anomalies[y_col], color='red')
    ax1.set_title('Anomaly')
    ax2.scatter(normal[x_col], normal[y_col], color='orange')
    ax2.set_title('Normal')
    plt.xlabel(x_col)
    plt.ylabel(y_col)
    plt.show()

plt.scatter(df.Amount, df.Class)
plt.title("Transaction Amounts by Class")
plt.ylabel("Class")
plt.yticks(range(2), ["Normal" " Anomlay"])
plt.xlabel("Transcation Amouts ($) ")
plt.show()

